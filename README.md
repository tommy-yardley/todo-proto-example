# TODO

This project aims to show simple things you can do with protobuf.

There is an extension to this where I use gRPC to enhance the previous solution by creating a gRPC server and turning the cli into a client/server combo.

## Requirements

### Protoc

`go get -u github.com/golang/protobuf/protoc-gen-go`

### gRPC

`go get -u google.golang.org/grpc`

## How to generate pb.go files

With gRPC plugin:

`protoc --go_out=. <path_to_protos>`


Without gRPC plugin:

`protoc --go_out=plugins=grpc:. <path_to_protos>`

## How to run the proto-only example

In a terminal

```
cd proto-only
go run cmd/todo/main.go <subcommand> <potential arguments>
```

## How to run the grpc-proto example

In one terminal run the server:

```
cd grpc-proto
go run server/main.go
```


In another terminal run the client:

```
cd grpc-proto
go run client/main.go <subcommand> <potential arguments>
```

## Subcommands

### Add

Usage: `<command> add <description>`

This will take the string of words after `add` and create a single task out of it and add it to the database

### Complete

Usage: `<command> complete <task id>`

This will take the given id of a task and mark it as complete. To find a tasks id, use the `list` subcommand

### List

Usage: `<command> list`

This will list all the tasks needed to be completed, along with their ids
