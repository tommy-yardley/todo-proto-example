package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/tommy-yardley/todo-proto-example/grpc-proto/protos"
	grpc "google.golang.org/grpc"
)

func main() {
	//grab the arguments passed after the executable
	flag.Parse()
	if flag.NArg() < 1 {
		fmt.Fprintln(os.Stderr, "Not enough arguments\nFunctionality: <command> <subcommand>\n	Avaliable subcommands: add, complete, list")
		os.Exit(1)
	}

	//dial the grpc server and set up a connection
	connection, err := grpc.Dial("localhost:5050", grpc.WithInsecure())
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to connect to server: %v\n", err)
		os.Exit(1)
	}
	todoClient := todo.NewTodoClient(connection)

	//switch on the first argument - which is the subcommand
	switch subcmd := flag.Arg(0); subcmd {
	case "add":
		if flag.NArg() == 1 {
			fmt.Fprintln(os.Stderr, "Please specify the task to add\nFunctionality: <command> add <description of task to add>")
			os.Exit(1)
		}
		err = add(context.Background(), todoClient, strings.Join(flag.Args()[1:], " "))
	case "complete":
		if flag.NArg() == 1 {
			fmt.Fprintln(os.Stderr, "Please specify the task id to complete\nFunctionality: <command> complete <task id>")
			os.Exit(1)
		}
		position, err := strconv.Atoi(flag.Args()[1])
		if err != nil {
			break
		}
		err = complete(context.Background(), todoClient, position)
	case "list":
		err = list(context.Background(), todoClient)
	default:
		err = fmt.Errorf("Unknown subcommand %s\nFunctionality: <command> <subcommand>\n	Avaliable subcommands: add, complete, list", subcmd)
	}

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func add(context context.Context, todoClient todo.TodoClient, desc string) error {
	taskDesc := &todo.Desc{
		Desc: desc,
	}

	_, err := todoClient.Add(context, taskDesc)
	if err != nil {
		return fmt.Errorf("Failed to add task on the server: %v", err)
	}
	return nil
}

func list(context context.Context, todoClient todo.TodoClient) error {
	//request list of tasks from the server
	tasks, err := todoClient.List(context, &todo.Nothing{})
	if err != nil {
		return fmt.Errorf("Failed to fetch tasks from the server: %v", err)
	}

	//loop through the tasks and print out to the console
	position := 0
	for _, task := range tasks.Tasks {
		//check if the task is done or not
		if task.Done {
			fmt.Printf("id: %d --> [X] %s\n", position, task.Desc)
		} else {
			fmt.Printf("id: %d --> [] %s\n", position, task.Desc)
		}
		position++
	}
	return nil
}

func complete(context context.Context, todoClient todo.TodoClient, position int) error {
	id := &todo.TaskId{
		Id: uint64(position),
	}
	_, err := todoClient.Complete(context, id)
	if err != nil {
		return fmt.Errorf("Failed to complete task on the server: %v", err)
	}
	return nil
}
