package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strings"

	"bitbucket.org/tommy-yardley/todo-proto-example/grpc-proto/protos"
	"github.com/golang/protobuf/proto"
	"golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

//don't judge me
const (
	databasePath      = "database.pb"
	messageLengthSize = uint64(8)
)

//this needs to implement ALL api calls
type todoService struct{}

func main() {
	//create new grpc server
	grpcServer := grpc.NewServer()

	//register our todoService with the grpc server
	var todoService todoService
	todo.RegisterTodoServer(grpcServer, todoService)

	//create a listener to allow the grpc server to serve from it
	listener, err := net.Listen("tcp", "localhost:5050")
	if err != nil {
		log.Fatalf("Failed to listen %v", err)
	}
	grpcServer.Serve(listener)
}

func (todoService) Add(context context.Context, desc *todo.Desc) (*todo.Task, error) {
	//create a new Task
	task := &todo.Task{
		Desc: desc.GetDesc(),
		Done: false,
	}

	//use proto to encode task
	bytes, err := proto.Marshal(task)
	if err != nil {
		return nil, fmt.Errorf("Failed to encode task: %v", err)
	}

	//open write-only file, if it doesn't already exist make it, otherwise append
	file, err := os.OpenFile(databasePath, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return nil, fmt.Errorf("Failed to open file %s: %v", databasePath, err)
	}

	//we have no way of knowing in our database when one message ends and another
	//starts, so we can write the length of the bytes being written so we know
	//when the next message should start!
	if err := binary.Write(file, binary.LittleEndian, uint64(len(bytes))); err != nil {
		return nil, fmt.Errorf("Failed to write length of message: %v", err)
	}

	//write our message to the database
	_, err = file.Write(bytes)
	if err != nil {
		return nil, fmt.Errorf("Failed to write task to database: %v", err)
	}

	//I think if I complied all of the worlds go code ~ 80% will be error handling
	if err := file.Close(); err != nil {
		return nil, fmt.Errorf("Failed to close file %s: %v", databasePath, err)
	}

	return task, nil
}

func (todoService) List(context context.Context, nothing *todo.Nothing) (*todo.TaskList, error) {
	//read the database into a byte[]
	database, err := ioutil.ReadFile(databasePath)
	if err != nil {
		return nil, fmt.Errorf("Failed to read %s: %v", databasePath, err)
	}

	//loop through the database add each task to the tasklist and then return it
	var tasksList todo.TaskList
	for {
		if uint64(len(database)) < messageLengthSize {
			return &tasksList, nil
		}

		var messageLength uint64
		if err := binary.Read(bytes.NewReader(database[:messageLengthSize]), binary.LittleEndian, &messageLength); err != nil {
			return nil, fmt.Errorf("Failed to decode message length: %v", err)
		}

		//remove the bytes corresponding to the message length
		database = database[messageLengthSize:]

		//decode the task
		var task todo.Task
		if err := proto.Unmarshal(database[:messageLength], &task); err != nil {
			return nil, fmt.Errorf("Failed to read task: %v", err)
		}

		//remove the bytes corresponding to the message
		database = database[messageLength:]

		tasksList.Tasks = append(tasksList.Tasks, &task)
	}
}

func (todoService) Complete(context context.Context, position *todo.TaskId) (*todo.Nothing, error) {
	nothing := &todo.Nothing{}

	//read the database into a byte[]
	database, err := ioutil.ReadFile(databasePath)
	if err != nil {
		return nil, fmt.Errorf("Failed to read %s: %v", databasePath, err)
	}

	//loop through the database until we reach our position
	bytePos := uint64(0)
	currentPosition := uint64(0)
	for {
		if uint64(len(database)) < messageLengthSize {
			return nil, fmt.Errorf("The database is empty - cannot complete task")
		}

		var messageLength uint64
		if err := binary.Read(bytes.NewReader(database[bytePos:bytePos+messageLengthSize]), binary.LittleEndian, &messageLength); err != nil {
			return nil, fmt.Errorf("Failed to decode message length between positions [%d, %d]: %v", bytePos, bytePos+messageLengthSize, err)
		}

		//move along bytes corresponding to the message length size
		bytePos += messageLengthSize

		if position.GetId() == currentPosition {
			//decode the task
			var task todo.Task
			oldTaskBytes := database[bytePos : bytePos+messageLength]
			if err := proto.Unmarshal(oldTaskBytes, &task); err != nil {
				return nil, fmt.Errorf("Failed to read task: %v", err)
			}

			//set task to done
			task.Done = true

			//encode the new task
			newTaskBytes, err := proto.Marshal(&task)
			if err != nil {
				return nil, fmt.Errorf("Failed to encode new task: %v", err)
			}

			//replace previous message size with the new message size
			lengthBuf := new(bytes.Buffer)
			if err := binary.Write(lengthBuf, binary.LittleEndian, uint64(len(newTaskBytes))); err != nil {
				return nil, fmt.Errorf("Failed to write length of message to length buffer: %v", err)
			}
			copy(database[bytePos-messageLengthSize:bytePos], lengthBuf.Bytes())

			//pretty gross by convert database to string representation and replace the oldtaskbytes with the new
			newDatabase := strings.Replace(string(database), string(oldTaskBytes), string(newTaskBytes), -1)

			//replace the old database with the new
			if err = ioutil.WriteFile(databasePath, []byte(newDatabase), 0); err != nil {
				return nil, fmt.Errorf("Failed to write to database: %v", err)
			}

			return nothing, nil
		}

		//move along the bytes corresponding to the message length
		bytePos += uint64(messageLength)

		currentPosition++
	}

	return nil, fmt.Errorf("There is no task with id: %d", position)
}
