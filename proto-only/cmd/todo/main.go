package main

import (
	"bytes"
	"encoding/binary"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/tommy-yardley/todo-proto-example/proto-only/protos"
	"github.com/golang/protobuf/proto"
)

//don't judge me
const (
	databasePath      = "database.pb"
	messageLengthSize = uint64(8)
)

func main() {
	//grab the arguments passed after the executable
	flag.Parse()
	if flag.NArg() < 1 {
		fmt.Fprintln(os.Stderr, "Not enough arguments\nFunctionality: <command> <subcommand>\n	Avaliable subcommands: add, complete, list")
		os.Exit(1)
	}

	//switch on the first argument - which is the subcommand
	var err error
	switch subcmd := flag.Arg(0); subcmd {
	case "add":
		if flag.NArg() == 1 {
			fmt.Fprintln(os.Stderr, "Please specify the task to add\nFunctionality: <command> add <description of task to add>")
			os.Exit(1)
		}
		err = add(strings.Join(flag.Args()[1:], " "))
	case "complete":
		if flag.NArg() == 1 {
			fmt.Fprintln(os.Stderr, "Please specify the task id to complete\nFunctionality: <command> complete <task id>")
			os.Exit(1)
		}
		position, err := strconv.Atoi(flag.Args()[1])
		if err != nil {
			break
		}
		err = complete(position)
	case "list":
		err = list()
	default:
		err = fmt.Errorf("Unknown subcommand %s\nFunctionality: <command> <subcommand>\n	Avaliable subcommands: add, complete, list", subcmd)
	}

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func add(desc string) error {
	//create a new Task
	task := &todo.Task{
		Desc: desc,
		Done: false,
	}

	//use proto to encode task
	bytes, err := proto.Marshal(task)
	if err != nil {
		return fmt.Errorf("Failed to encode task: %v", err)
	}

	//open write-only file, if it doesn't already exist make it, otherwise append
	file, err := os.OpenFile(databasePath, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return fmt.Errorf("Failed to open file %s: %v", databasePath, err)
	}

	//we have no way of knowing in our database when one message ends and another
	//starts, so we can write the length of the bytes being written so we know
	//when the next message should start!
	if err := binary.Write(file, binary.LittleEndian, uint64(len(bytes))); err != nil {
		return fmt.Errorf("Failed to write length of message: %v", err)
	}

	//write our message to the database
	_, err = file.Write(bytes)
	if err != nil {
		return fmt.Errorf("Failed to write task to database: %v", err)
	}

	//I think if I complied all of the worlds go code ~ 80% will be error handling
	if err := file.Close(); err != nil {
		return fmt.Errorf("Failed to close file %s: %v", databasePath, err)
	}

	return nil
}

func list() error {
	//read the database into a byte[]
	database, err := ioutil.ReadFile(databasePath)
	if err != nil {
		return fmt.Errorf("Failed to read %s: %v", databasePath, err)
	}

	//loop through the database and print out each task to the console
	position := 0
	for {
		if uint64(len(database)) < messageLengthSize {
			return nil
		}

		var messageLength uint64
		if err := binary.Read(bytes.NewReader(database[:messageLengthSize]), binary.LittleEndian, &messageLength); err != nil {
			return fmt.Errorf("Failed to decode message length: %v", err)
		}

		//remove the bytes corresponding to the message length
		database = database[messageLengthSize:]

		//decode the task
		var task todo.Task
		if err := proto.Unmarshal(database[:messageLength], &task); err != nil {
			return fmt.Errorf("Failed to read task: %v", err)
		}

		//remove the bytes corresponding to the message
		database = database[messageLength:]

		//check if the task is done or not
		if task.Done {
			fmt.Printf("id: %d --> [X] %s\n", position, task.Desc)
		} else {
			fmt.Printf("id: %d --> [] %s\n", position, task.Desc)
		}
		position++
	}
}

func complete(position int) error {
	//read the database into a byte[]
	database, err := ioutil.ReadFile(databasePath)
	if err != nil {
		return fmt.Errorf("Failed to read %s: %v", databasePath, err)
	}

	//loop through the database until we reach our position
	bytePos := uint64(0)
	currentPosition := 0
	for {
		if uint64(len(database)) < messageLengthSize {
			return fmt.Errorf("The database is empty - cannot complete task")
		}

		var messageLength uint64
		if err := binary.Read(bytes.NewReader(database[bytePos:bytePos+messageLengthSize]), binary.LittleEndian, &messageLength); err != nil {
			return fmt.Errorf("Failed to decode message length between positions [%d, %d]: %v", bytePos, bytePos+messageLengthSize, err)
		}

		//move along bytes corresponding to the message length size
		bytePos += messageLengthSize

		if position == currentPosition {
			//decode the task
			var task todo.Task
			oldTaskBytes := database[bytePos : bytePos+messageLength]
			if err := proto.Unmarshal(oldTaskBytes, &task); err != nil {
				return fmt.Errorf("Failed to read task: %v", err)
			}

			//set task to done
			task.Done = true

			//encode the new task
			newTaskBytes, err := proto.Marshal(&task)
			if err != nil {
				return fmt.Errorf("Failed to encode new task: %v", err)
			}

			//replace previous message size with the new message size
			lengthBuf := new(bytes.Buffer)
			if err := binary.Write(lengthBuf, binary.LittleEndian, uint64(len(newTaskBytes))); err != nil {
				return fmt.Errorf("Failed to write length of message to length buffer: %v", err)
			}
			copy(database[bytePos-messageLengthSize:bytePos], lengthBuf.Bytes())

			//pretty gross by convert database to string representation and replace the oldtaskbytes with the new
			newDatabase := strings.Replace(string(database), string(oldTaskBytes), string(newTaskBytes), -1)

			//replace the old database with the new
			if err = ioutil.WriteFile(databasePath, []byte(newDatabase), 0); err != nil {
				return fmt.Errorf("Failed to write to database: %v", err)
			}

			fmt.Printf("Completed task with id: %d", position)

			return nil
		}

		//move along the bytes corresponding to the message length
		bytePos += uint64(messageLength)

		currentPosition++
	}

	return fmt.Errorf("There is no task with id: %d", position)
}
